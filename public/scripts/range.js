var elem = document.getElementsByClassName('range');
var rangeValue = function (elem, target) {
    return function(evt){
        target.value = elem.value;
    }
}

for(var i = 0, max = elem.length; i < max; i++){
    range = elem[i].getElementsByTagName('input')[0];
    number = elem[i].getElementsByTagName('input')[1];
    range.addEventListener('input', rangeValue(range, number));
    number.addEventListener('input', rangeValue(number, range));
}