var arrShortSword = [
  {value:"1", text:"test1-1"},
  {value:"2", text:"test1-2"},
  {value:"3", text:"test1-3"}
];
var arrAxe = [
  {value:"1", text:"test2-1"},
  {value:"2", text:"test2-2"},
  {value:"3", text:"test2-3"}
];
var arrBow = [
  {value:"1", text:"test3-1"},
  {value:"2", text:"test3-2"},
  {value:"3", text:"test3-3"}
];

function setSelectBox(){
    var weapon = document.getElementById('weapon');
    var material = document.getElementById('material');
    var arts = document.getElementById('arts');

    material.disabled=false;
    arts.disabled=false;
    
    switch(weapon.selectedIndex){
        case 1:
            setArry(material, arrShortSword);
        break;
        case 2:
            setArry(material, arrAxe);
        break;
        case 3:
            setArry(material, arrBow);
        break;
    }
}

function setArry(obj, arr){
    while(obj.length>1){
    obj.removeChild(obj.lastChild);
    }
    for(var i=0;i<arr.length;i++){
    let op = document.createElement("option");
    op.value = arr[i].value;
    op.text = arr[i].text;
    obj.appendChild(op);
    obj.selectedIndex=0;
  }
}

